stages:
  - build
  - test
  - deploy


build-pypi-package:
  stage: build
  image: python:3.9-slim-bullseye
  needs: []
  before_script:
    - pip install --upgrade pip
    - pip install --upgrade build
  script:
    - python -m build
  artifacts:
    expire_in: 1 day
    paths:
      - dist/


doc:
    # build docs
    stage: build
    image: registry.gitlab.com/taurus-org/taurus-docker:conda-3.9
    needs: []
    variables:
        PUBLIC_URL: /-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/public
    before_script:
        - pip install sphinx sphinx-rtd-theme
    script:
        - xvfb-run sphinx-build -qW doc/source/ public
    artifacts:
        paths:
            - public
    environment:
        name: Docs-dev
        url: "https://$CI_PROJECT_NAMESPACE.gitlab.io/-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/public/index.html"


flake8_and_black:
    # check style with flake8
    stage: test    
    image: python:3.9-slim-bullseye
    needs: []
    before_script:
        - pip install flake8==3.9.2 black==22.3.0
    script:
        # check with flake8
        - flake8 .
        # check with black
        - black --check --diff .


tests:
    # run test suite with various py versions
    parallel:
        matrix:
            - TAURUS_TAG: ["bullseye", "buster", "stretch"]
              # disable bullseye pyside2 tests until they are green
              # QT_API: ["pyqt5", "pyside2"]
              QT_API: ["pyqt5"]
            - TAURUS_TAG: ["conda-3.9"]
              QT_API: ["pyqt5", "pyside2"]
            - TAURUS_TAG: ["conda-3.6", "conda-3.7", "conda-3.8"]
              QT_API: ["pyqt5"]
    stage: test
    needs: ["build-pypi-package"]
    variables:
        GIT_STRATEGY: none
        PYTEST_QT_API: $QT_API
    image:
        name: registry.gitlab.com/taurus-org/taurus-docker:$TAURUS_TAG
    before_script:
        - python3 -m pip install dist/taurus-*.whl
    script:
        - INST_DIR=$(python3 -c 'import taurus; print(taurus.__file__.rsplit("/",1)[0])')
        - python3 -m pytest --junitxml=report.xml -n3 --forked -v $INST_DIR
    artifacts:
        when: always
        reports:
            junit: report.xml

pages:
    # deploy docs
    stage: deploy
    image: alpine
    variables:
        GIT_STRATEGY: none
    script:
        - echo "Deploying already-built docs"
    artifacts:
        paths:
            - public
    only:
        variables:
            - $CI_COMMIT_TAG =~ /^[0-9]+\.[0-9]+\.[0-9]+.*$/


deploy_to_pypi:
    stage: deploy
    image: python:3.9-slim-bullseye
    variables:
        GIT_STRATEGY: none
    before_script:
        - pip install twine
    script:
        - twine upload dist/*
    only:
        variables:
            - $CI_COMMIT_TAG =~ /^[0-9]+\.[0-9]+\.[0-9]+.*$/
